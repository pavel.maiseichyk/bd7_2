import asyncio
import os
from flask import Flask, request
from Neo4jConnector import Neo4jConnector

uri = os.environ.get("URI")
username = os.environ.get("USERNAME")
password = os.environ.get("PASSWORD")

db = Neo4jConnector(uri, username, password)
app = Flask(__name__)


@app.route('/', methods=['GET'])
async def index():
    return 'Hello, Flask!'


@app.route('/employees', methods=['GET'])
async def get_employees():
    sort_by = request.args.get('sort_by', 'name')
    filter_field = request.args.get('filter_field')
    filter_value = request.args.get('filter_value')
    return await db.get_employees(sort_by, filter_field, filter_value)


@app.route('/employees', methods=['POST'])
async def add_employee():
    employee = request.get_json()
    return await db.upload_new_employee(employee)


@app.route('/employees/<id>', methods=['PUT'])
async def update_employee(id):
    data = request.get_json()
    return await db.update_employee(id, data)


@app.route('/employees/<id>', methods=['DELETE'])
async def delete_employee(id):
    return await db.delete_employee(id)


@app.route('/employees/<id>/subordinates', methods=['GET'])
async def get_subordinates(id):
    return await db.get_subordinates(id)


@app.route('/employees/<id>/department', methods=['GET'])
async def get_employee_department(id):
    return await db.get_employee_department(id)


@app.route('/departments', methods=['GET'])
async def get_departments():
    sort_by = request.args.get('sort_by', 'name')
    filter_field = request.args.get('filter_field')
    filter_value = request.args.get('filter_value')
    return await db.get_departments(sort_by, filter_field, filter_value)


@app.route('/departments/<id>/employees', methods=['GET'])
async def get_department_employees(id):
    return await db.get_department_employees(id)


async def main():
    app.run()


if __name__ == '__main__':
    asyncio.run(main())
