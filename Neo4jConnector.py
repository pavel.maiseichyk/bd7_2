from flask import jsonify
from neo4j import GraphDatabase


class Neo4jConnector:
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    async def close(self):
        self.driver.close()

    async def execute_query(self, query):
        return self.driver.session().run(query)

    async def get_employees(self, sort_by, filter_field, filter_value):
        query = "MATCH (e:Employee)\n"
        if filter_field and filter_value:
            query += f"WHERE e.{filter_field} CONTAINS '{filter_value}'\n"
        query += "RETURN e\n"
        query += f"ORDER BY e.{sort_by}"

        result = await self.execute_query(query)
        employees = [record.data()["e"] for record in result]
        return jsonify(employees)

    async def upload_new_employee(self, employee):
        name = employee.get("name")
        surname = employee.get("surname")
        department = employee.get("department")
        position = employee.get("position")

        if not name or not surname or not department or not position:
            return jsonify({"error": "Bad request"}), 400

        user_exists = await self.execute_query(f"MATCH (e:Employee {{name: '{name}', surname: '{surname}'}}) RETURN e")
        if [record.data() for record in user_exists]:
            return jsonify({"error": "Employee already exists"}), 409

        department_exists = await self.execute_query(f"MATCH (d:Department {{name: '{department}'}}) RETURN d")
        if not [record.data() for record in department_exists]:
            return jsonify({"error": "Department doesn't exist"}), 400

        query = (
            f"CREATE (e:Employee {{name: '{name}', surname: '{surname}', position: '{position}'}})\n"
            f"WITH e\n"
            f"MATCH (d:Department {{name: '{department}'}})\n"
            f"CREATE(e)-[:WORKS_IN]->(d)"
            f"RETURN e"
        )
        result = await self.execute_query(query)

        try:
            employee = [record.data()["e"] for record in result][0]
        except IndexError:
            return jsonify({"error": "Bad request"}), 400

        return jsonify(employee), 200

    async def update_employee(self, id, data):
        query = f"MATCH (e:Employee) WHERE id(e) = {id}\n"

        if "name" in data:
            query += f"SET e.name = '{data['name']}'\n"
        if "surname" in data:
            query += f"SET e.surname = '{data['surname']}'\n"
        if "position" in data:
            query += f"SET e.position = '{data['position']}'\n"
        if "department" in data:
            if "department" in data:
                query += (
                    f"WITH e\n"
                    f"MATCH (e)-[r:WORKS_IN]->(:Department)\n"
                    f"DELETE r\n"
                    f"WITH e\n"
                    f"MATCH (d:Department {{name: '{data['department']}'}})\n"
                    f"MERGE (e)-[:WORKS_IN]->(d)\n"
                )
        query += "RETURN e"

        result = await self.execute_query(query)

        try:
            employee = [record.data()["e"] for record in result][0]
        except IndexError:
            return jsonify({"error": "No employee found"}), 404

        return jsonify(employee), 200

    async def delete_employee(self, id):
        query = (
            f"MATCH (e:Employee) WHERE id(e) = {id}\n"
            f"OPTIONAL MATCH (e)-[:MANAGES]->(d:Department)\n"
            f"WITH e, d, count(d) as dept_count\n"
            f"DETACH DELETE e, d\n"
            f"RETURN e, dept_count"
        )

        result = await self.execute_query(query)
        deleted_employees, deleted_departments = result.single()
        if deleted_employees == 0:
            return jsonify({"error": "Employee not found"}), 404
        else:
            return jsonify({"message": f"Employee and {deleted_departments} department(s) deleted successfully"})

    async def get_subordinates(self, id):
        query = (
            f"MATCH (e:Employee)-[:MANAGES]->(d:Department)<-[:WORKS_IN]-(s:Employee)\n"
            f"WHERE id(e) = {id} AND NOT (s)-[:MANAGES]->(:Department)\n"
            f"RETURN s"
        )
        result = await self.execute_query(query)
        subordinates = [record.data()["s"] for record in result]
        return jsonify(subordinates), 200

    async def get_employee_department(self, id):
        query = (
            f"MATCH (e:Employee)-[:WORKS_IN]->(d:Department) WHERE id(e) = {id}\n"
            f"OPTIONAL MATCH (d)<-[:MANAGES]-(m:Employee)\n"
            f"RETURN d.name as department_name, count(e) as employee_count, m.name as manager_name"
        )
        result = await self.execute_query(query)
        try:
            department_info = [record.data() for record in result][0]
        except IndexError:
            return jsonify({"error": "No department found for this employee"}), 404

        return jsonify(department_info), 200

    async def get_departments(self, sort_by, filter_field, filter_value):
        query = ("MATCH (d:Department)<-[:WORKS_IN]-(e:Employee)\n"
                 "WITH d, d.name as name, count(e) as employee_count\n")
        if filter_field and filter_value:
            query += f"WHERE {filter_field} = {filter_value}\n"
        query += "RETURN name, employee_count\n"
        query += f"ORDER BY {sort_by}"

        result = await self.execute_query(query)
        departments = [record.data() for record in result]
        return jsonify(departments), 200

    async def get_department_employees(self, id):
        query = f"MATCH (d:Department)<-[:WORKS_IN]-(e:Employee) WHERE id(d) = {id} RETURN e"
        result = await self.execute_query(query)
        employees = [record.data()["e"] for record in result]
        return jsonify(employees), 200